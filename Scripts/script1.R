## Créer un objet :
fleur <- "iris"
print(fleur)
fleur

## Définir un vecteur numérique x :
x <- c(2, 5.9, 6.2, 8.3)
print(x) ## ou plus simplement : x

## Accéder aux composantes de x :
x[1]       # afficher le 1er élément de x
x[3]       # afficher le 3e élément de x
x[1, 3]    # provoque une erreur !
x[c(1, 3)] # afficher les 1er et 3e éléments

## Création de suites de valeurs :
1:9 # c(1, 2, 3, 4, 5, 6, 7, 8, 9)
seq(from = 2, to = 5, by = 0.5)

## Facteur :
fac <- c("a", "a", "b", "a", "b")
print(fac)   # à ce stade, fac = vecteur de caractères
fac <- as.factor(fac)
print(fac)   # désormais, fac = facteur
levels(fac)  # liste des modalités
nlevels(fac) # nb de modalités
table(fac)   # décompte des modalités

## Matrice :
M <- matrix(c(1, 2, 3, 4), ncol = 2)
print(M)
M[1, 2]
M[1, ]
M[, 2]

## Liste :
L <- list("a", 1:5, M)
print(L)
L[[2]]

## Opérations sur les vecteurs :
x <- c(10, 12, 8, 10)
mean(x)
length(x) # nb d'éléments
max(x)
min(x)
which.min(x)

y <- c(9, 14)
z <- c(x, y)
print(z)

z[-3]
sort(z)
z/2

## Attribuer des noms aux composantes d'un vecteur :
print(z)
names(z) <- c("Jim", "Eva", "Ana", "Dan", "Ron", "Leo")
print(z)
z[3]
z["Ana"]
